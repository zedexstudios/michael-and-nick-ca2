// MUD Programming
// Ron Penton
// (C)2003
// Demo10-01.cpp - SimpleMUD - Enemies, Enemy Databases, and the Game Loop
// 
// Michael here

#include <sstream>

#include "SocketLib/SocketLib.h"

#include "CellSwapCA2/ItemDatabase.h"
#include "CellSwapCA2/PlayerDatabase.h"
#include "CellSwapCA2/RoomDatabase.h"
#include "CellSwapCA2/StoreDatabase.h"
#include "CellSwapCA2/EnemyDatabase.h"

#include "CellSwapCA2/Logon.h"
#include "CellSwapCA2/Game.h"
#include "CellSwapCA2/GameLoop.h"

#include "CellSwapCA2/SimpleMUDLogs.h"


using namespace SocketLib;
using namespace CellSwapCA2;


int main()
{
    try
    {
        GameLoop gameloop;

        ListeningManager<Telnet, Logon> lm;
        ConnectionManager<Telnet, Logon> cm( 128, 60, 65536 );

        lm.SetConnectionManager( &cm );
        lm.AddPort( 5100 );

        while( Game::Running() )
        {
            lm.Listen();
            cm.Manage();
            gameloop.Loop();
            ThreadLib::YieldThread();
        }
    }

    catch( SocketLib::Exception& e )
    {
        ERRORLOG.Log( "Fatal Socket Error: " + e.PrintError() );
    }

    catch( ThreadLib::Exception& )
    {
        ERRORLOG.Log( "Fatal Thread Error" );
    }

    catch( std::exception& e )
    {
        ERRORLOG.Log( "Standard Error: " + std::string( e.what() ) );
    }

    catch( ... )
    {
        ERRORLOG.Log( "Unspecified Error" );
    }



}

