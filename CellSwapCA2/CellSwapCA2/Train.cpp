// MUD Programming
// Ron Penton
// (C)2003
// Train.cpp - This class is the training handler for SimpleMUD.
// 
// 

#include "../BasicLib/BasicLib.h"
#include "Train.h"
#include "PlayerDatabase.h"

using namespace SocketLib;

namespace CellSwapCA2
{

// ------------------------------------------------------------------------
//  This handles incomming commands. Anything passed into this function
//  is assumed to be a complete command from a client.
// ------------------------------------------------------------------------
void Train::Handle( string p_data )
{
    using namespace BasicLib;

    p_data = BasicLib::LowerCase( ParseWord( p_data, 0 ) );

    Player& p = *m_player;

    if( p_data == "enter" )
    {
        // save the player to disk
        PlayerDatabase::SavePlayer( p.ID() );

        // go back to the previous handler
        p.Conn()->RemoveHandler();
        return;
    }
	if( p_data == "choose")
	{
		p.StatPoints() = 36;
		p.SetBaseAttr(0, 1);
		p.SetBaseAttr(1, 1);
		p.SetBaseAttr(2, 1);
		p.SetBaseAttr(4, 1);
		p.SetBaseAttr(5, 1);
		p.SetBaseAttr(6, 1);
		p.SetBaseAttr(7, 1);
	}

    char n = p_data[0];

    if( n == '1') // Hacker
    {
        if( p.StatPoints() == 36 )
        {
            p.StatPoints() = 0;
            p.AddToBaseAttr( 0, 5 ); //SPEECH
			p.AddToBaseAttr( 1, 3 ); //TRUST
			p.AddToBaseAttr( 2, 3 ); //ENCUMBRANCE
			p.AddToBaseAttr( 4, 6 ); //SKILL
			p.AddToBaseAttr( 5, 6 ); //LUCK
			p.AddToBaseAttr( 6, 4 ); //PERSUASION
			p.AddToBaseAttr( 7, 2 ); //STAMINA
        }
    }
	if( n == '2') // Muscle
    {
        if( p.StatPoints() == 36 )
        {
            p.StatPoints() = 0;
            p.AddToBaseAttr( 0, 2 );
			p.AddToBaseAttr( 1, 3 );
			p.AddToBaseAttr( 2, 6 );
			p.AddToBaseAttr( 4, 5 );
			p.AddToBaseAttr( 5, 4 );
			p.AddToBaseAttr( 6, 5 );
			p.AddToBaseAttr( 7, 4 );
        }
    }
	if( n == '3') // Charmer
    {
        if( p.StatPoints() == 36 )
        {
            p.StatPoints() = 0;
            p.AddToBaseAttr( 0, 7 );
			p.AddToBaseAttr( 1, 5 );
			p.AddToBaseAttr( 2, 2 );
			p.AddToBaseAttr( 4, 3 );
			p.AddToBaseAttr( 5, 5 );
			p.AddToBaseAttr( 6, 5 );
			p.AddToBaseAttr( 7, 2 );
        }
    }

    PrintStats( true );

}


// ------------------------------------------------------------------------
//  This notifies the handler that there is a new connection
// ------------------------------------------------------------------------
void Train::Enter()
{
    Player& p = *m_player;

    p.Active() = false;

    if( p.Newbie() )
    {
        p.SendString( magenta + bold + 
            "Welcome to CellSwap, " + p.Name() + "!\r\n" + 
            "You must choose your class,\r\n" +
            "before you enter the prison.\r\n\r\n" );
        p.Newbie() = false;
    }

    PrintStats( false );
}



// ------------------------------------------------------------------------
//  This function prints out your statistics.
// ------------------------------------------------------------------------
void Train::PrintStats( bool p_clear )
{
    using BasicLib::tostring;

    Player& p = *m_player;

    if( p_clear )
    {
        p.SendString( clearscreen );
    }


    p.SendString( white + bold + 
        "--------------------------------- Your Stats ----------------------------------\r\n" +
        "Player:			" + p.Name() + "\r\n" + 
        "Level:				" + tostring( p.Level() ) + "\r\n" +
        "Stat Points Left:	" + tostring( p.StatPoints() ) + "\r\n" + 
        "1) Speech:			" + tostring( p.GetAttr( SPEECH ) ) + "\r\n" +
        "2) Trust:			" + tostring( p.GetAttr( TRUST ) ) + "\r\n" +
		"3) Encumbrance:			" + tostring( p.GetAttr( ENCUMBRANCE ) ) + "\r\n" +
		"4) Skill:		        " + tostring( p.GetAttr( SKILL ) ) + "\r\n" +
		"5) Luck:			" + tostring( p.GetAttr( LUCK ) ) + "\r\n" +
		"6) Persuasion:		        " + tostring( p.GetAttr( PERSUASION ) ) + "\r\n" +
		"7) Stamina:			" + tostring( p.GetAttr( STAMINA ) ) + "\r\n" +
        bold + 
        "-------------------------------------------------------------------------------\r\n" +
        "Enter 1 to choose the Hacker class: \r\n" +
		"Enter 2 to choose the Muscle class: \r\n" + 
		"Enter 3 to choose the Charmer class: \r\n" + 
		"Type 'enter' if you are happy with your choice. \r\n" +
		"Type 'choose' if you wish to re-assign your stat points.");
}


}   // end namespace CellSwapCA2