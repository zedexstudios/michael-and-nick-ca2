#include <fstream>
#include "../BasicLib/BasicLib.h"
#include "SimpleMUDLogs.h"
#include "QuestionDatabase.h"

using BasicLib::LowerCase;
using BasicLib::tostring;

namespace CellSwapCA2
{

// declare the static map of the Question database.
	std::map<entityid, Question> EntityDatabase<Question>::m_map;


bool QuestionDatabase::Load()
{
    std::ifstream file( "questions/questions.list" );
    entityid id;
    std::string temp;

	int finalID = 0;

    while( file.good() )
    {
        file >> temp >> id;
        m_map[id].ID() = id;
        file >> m_map[id] >> std::ws;
		USERLOG.Log( "Loaded Question: " + m_map[id].Name() + " : " + m_map[id].Text());
		finalID = id;
    }	

	//for(int i = 1; i < finalID +1; i++){
	//	m_question.push_back(m_map[i]);
	//}

    return true;
}








}   // end namespace SimpleMUD
