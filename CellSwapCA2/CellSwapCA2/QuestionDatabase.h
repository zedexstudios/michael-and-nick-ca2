// MUD Programming
// Ron Penton
// (C)2003
// QuestionDatabase.h - The class that stores all of the Question within the MUD
// 
// 

#ifndef CELLSWAPQUESTIONDATABASE_H
#define CELLSWAPQUESTIONDATABASE_H


#include <string>
#include <map>

#include "EntityDatabase.h"
#include "Question.h"
#include <list>
#include "DatabasePointer.h"

namespace CellSwapCA2
{


// --------------------------------------------------------------------
//  A database for items
// --------------------------------------------------------------------
class QuestionDatabase : public EntityDatabase<Question>
{
public:
		//std::map<entityid, Question> EntityDatabase<Question>::m_map;
	//static list<Question> m_question;
    static bool Load();

};  // end class QuestionDatabase


}   // end namespace CellSwapCA2

#endif

