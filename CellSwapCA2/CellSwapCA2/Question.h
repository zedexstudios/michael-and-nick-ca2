#ifndef CELLSWAPQUESTION_H
#define CELLSWAPQUESTION_H

#include <string>
#include <iostream>
using namespace std;
#include "../BasicLib/BasicLib.h"
#include "Entity.h"

using std::ostream;
using std::istream;

namespace CellSwapCA2
{

// --------------------------------------------------------------------
//  Class that stores Question info
// --------------------------------------------------------------------
class Question : public Entity
{
public:
    Question()
    {
        m_text = " ";
    }

	inline string& Text()               { return m_text; }
    friend istream& operator>>( istream& p_stream, Question& i );

protected:

	string m_text;

};  // end class Question


// --------------------------------------------------------------------
//  Extracts an Question in text form from a stream
// --------------------------------------------------------------------
inline istream& operator>>( istream& p_stream, Question& i )
{
    std::string temp;
	p_stream >> temp >> std::ws;     std::getline( p_stream, i.m_name );
	p_stream >> temp >> std::ws; std::getline( p_stream, i.m_text );
    return p_stream;
}

}   // end namespace CellSwapCA2

#endif