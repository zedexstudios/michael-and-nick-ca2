// MUD Programming
// Ron Penton
// (C)2003
// ItemDatabase.h - The class that stores all of the items within the MUD
// 
// 

#ifndef CELLSWAPITEMDATABASE_H
#define CELLSWAPITEMDATABASE_H


#include <string>
#include <map>

#include "EntityDatabase.h"
#include "Item.h"
#include "DatabasePointer.h"

namespace CellSwapCA2
{


// --------------------------------------------------------------------
//  A database for items
// --------------------------------------------------------------------
class ItemDatabase : public EntityDatabase<Item>
{
public:

    static bool Load();

};  // end class ItemDatabase


}   // end namespace CellSwapCA2

#endif
