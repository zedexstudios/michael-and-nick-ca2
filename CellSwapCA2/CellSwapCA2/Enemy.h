// MUD Programming
// Ron Penton
// (C)2003
// Enemy.h - The class defining Enemies in the CellSwapCA2
// 
// 


#ifndef ENEMY_H
#define ENEMY_H

#include <math.h>
#include <list>
#include "../BasicLib/BasicLib.h"
#include "DatabasePointer.h"
#include "Item.h"
#include "Question.h"

using std::ostream;
using std::istream;
using std::list;
using BasicLib::sint64;

namespace CellSwapCA2
{


typedef std::pair< item, int > loot;
typedef std::pair< question, int > questionAsk;

class EnemyTemplate : public Entity
{
public:

    EnemyTemplate();
    friend istream& operator>>( istream& p_stream, EnemyTemplate& t );

	//int m_level;
    int m_hitpoints;
    int m_skill;
    int m_luck;
	//bool m_intro;
    int m_persuasion;
    int m_stamina;
    int m_experience;
	//int m_questions;
    item m_weapon;
    money m_moneymin;
    money m_moneymax;

    list<loot> m_loot;
	list<questionAsk> m_questions;

	bool isDistracted;

};  // end class EnemyTemplate





class Enemy : public Entity
{
public:

    Enemy();

    int& HitPoints()            { return m_hitpoints; }
    room& CurrentRoom()         { return m_room; }
    sint64& NextAttackTime()    { return m_nextattacktime;}
	bool& Intro()                { return m_intro;}

	void settAttackTime(int val) { m_nextattacktime = val; }
    
    void LoadTemplate( enemytemplate p_template );

	//An Enemy can be 1 of 3 different levels or ranks
	//int Level();
    std::string& Name();
    int Skill();
    int Luck();
    int Persuasion();
    int Stamina();
    int Experience();
	//int Questions();
    item Weapon();
    money MoneyMin();
    money MoneyMax();
    list<loot>& LootList();
	list<questionAsk>& QuestionList();

	bool& isDistracted();


    friend ostream& operator<<( ostream& p_stream, const Enemy& t );
    friend istream& operator>>( istream& p_stream, Enemy& t );

protected:
    enemytemplate m_template;
    int m_hitpoints;
    room m_room;
    BasicLib::sint64 m_nextattacktime;
	bool m_intro;

};  // end class Enemy


istream& operator>>( istream& p_stream, EnemyTemplate& t );
ostream& operator<<( ostream& p_stream, const Enemy& t );
istream& operator>>( istream& p_stream, Enemy& t );


}   // end namespace CellSwapCA2


#endif
